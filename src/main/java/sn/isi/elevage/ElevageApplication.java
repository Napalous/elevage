package sn.isi.elevage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElevageApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElevageApplication.class, args);
	}

}
